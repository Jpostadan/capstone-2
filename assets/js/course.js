let params = new URLSearchParams(window.location.search);
let courseId = params.get('courseId');
let adminUser = localStorage.getItem('isAdmin');
let token = localStorage.getItem('token');

let courseName = document.querySelector('#courseName');
let courseDesc = document.querySelector('#courseDesc');
let coursePrice = document.querySelector('#coursePrice');
let enrollContainer = document.querySelector('#enrollContainer');

if (token == null) {
  alert('Please login first!');
  window.location.replace('login.html');
}
fetch(`https://pure-beyond-84785.herokuapp.com/api/course/${courseId}`)
  .then((res) => res.json())
  .then((data) => {
    courseName.innerHTML = `${data.name}`;
    courseDesc.innerHTML = `${data.description}`;
    coursePrice.innerHTML = `${data.price}`;
    enrollContainer.innerHTML = `
    <table class="bg-info table">
        <thead class="">
            <tr>
                <td>First Name</td>
                <td>Last Name</td>
            </tr>
        </thead>
        <tbody id="eData">
        </tbody>
    </table>
`;
    let eData = document.querySelector('#eData');
    if (adminUser === 'false') {
      enrollContainer.innerHTML = `<button id="enrollButton" class="btn btn-block btn-primary">Enroll</button>`;
      let token = localStorage.getItem('token');
      document.querySelector('#enrollButton').addEventListener('click', () => {
        fetch(`https://pure-beyond-84785.herokuapp.com/api/users/enroll`, {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
          },
          body: JSON.stringify({
            courseId: courseId,
          }),
        })
          .then((res) => res.json())
          .then((data) => {
            if (data) {
              alert('Enrollment Success!!');
              window.location.replace('./courses.html');
            } else {
              alert('Something went wrong');
            }
          });
      });
    } else {
      let enrolleeData = data.enrollees;
      if (enrolleeData != undefined && enrolleeData.length > 0) {
        enrolleeData.map((a) => {
          fetch(`https://pure-beyond-84785.herokuapp.com/api/users/${a.userId}`)
            .then((res) => res.json())
            .then((data) => {
              eData.innerHTML += `
              <tr>
              <td>${data.firstName}</td>
              <td>${data.lastName}</td>
            </tr>
              `;
            });
        });
      }
    }
  });
