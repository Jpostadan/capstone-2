let token = localStorage.getItem('token');
let profileContainer = document.querySelector('#profileContainer');

if (!token || token === null) {
  alert('No user logged in.');
  window.location.replace('./login.html');
} else {
  fetch('https://pure-beyond-84785.herokuapp.com/api/users/details', {
    headers: {
      authorization: `Bearer ${userToken}`,
    },
  })
    .then((res) => res.json())
    .then((data) => {
      console.log(data);

      profileContainer.innerHTML = `
      <div class="col-md-12">
        <section class="jumbotron my-5 bg-secondary">
        <h3 class=" text-white  text-center font-weight-bold">First Name: ${data.firstName}</h3>
        <h3 class=" text-white text-center font-weight-bold">Last Name: ${data.lastName}</h3>
        <h3 class=" text-white text-center font-weight-bold">Email: ${data.email}</h3>
        <h3 class=" text-white text-center font-weight-bold">MobileNO: ${data.mobileNo}</h3>
        <h3 class="mt-5 mb-3 text-white  font-weight-bold">Class History</h3>
        <table class="table">
          <thead>
            <tr>
              <th  class="text-white "> Course ID </th>
              <th  class="text-white "> Enrolled On </th>
              <th class=" text-white "> Status </th>
            </tr>
          </thead>
          <tbody id="courseData">
          </tbody>
        </table>
        </section>
      </div>`;
      if (data.isAdmin === true) {
        profileContainer.innerHTML = `<div class="col-md-12">
                            <section class="jumbotron my-5 bg-secondary">
                                <h2 class="text-center">Welcome Admin !!<h2>`;
      }
      let courseData = document.querySelector('#courseData');
      data.enrollments.forEach((enrollment) => {
        fetch(
          `https://pure-beyond-84785.herokuapp.com/api/course/${enrollment.courseId}`
        )
          .then((res) => res.json())
          .then((data) => {
            let date = new Date(enrollment.enrolledOn);
            let mm = date.getMonth() + 1;
            let dd = date.getDate();
            let yy = date.getFullYear();

            courseData.innerHTML += `
            <tr>
            <td class=" text-white ">${data.name}</td>
            <td class=" text-white " >${mm}-${dd}-${yy}</td>
            <td class=" text-white " >${enrollment.status}</td>
            </tr>
      `;
          });
      });
    });
}
