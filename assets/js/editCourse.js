let params = new URLSearchParams(window.location.search);
let courseId = params.get('courseId');
let isAdmin = localStorage.getItem('isAdmin');

let courseName = document.querySelector('#courseName');
let price = document.querySelector('#coursePrice');
let description = document.querySelector('#courseDescription');
fetch(`https://pure-beyond-84785.herokuapp.com/api/course/${courseId}`)
  .then((res) => {
    return res.json();
  })
  .then((data) => {
    courseName.placeholder = data.name;
    price.placeholder = data.price;
    description.placeholder = data.description;

    document.querySelector('#editCourse').addEventListener('submit', (e) => {
      e.preventDefault();

      let courseName = courseName.value;
      let desc = description.value;
      let priceValue = price.value;

      let token = localStorage.getItem('token');
      if (courseName == '' || desc == '' || priceValue == '') {
        alert('Information incomplete');
      } else {
        fetch('https://pure-beyond-84785.herokuapp.com/api/course/', {
          method: 'PUT',
          headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`,
          },
          body: JSON.stringify({
            courseId: courseId,
            name: courseName,
            description: desc,
            price: priceValue,
          }),
        })
          .then((res) => {
            return res.json();
          })
          .then((data) => {
            if (data === true) {
              window.location.replace('./courses.html');
            } else {
              alert('Something went wrong');
            }
          });
      }
    });
  });
