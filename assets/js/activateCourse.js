let params = new URLSearchParams(window.location.search);
let courseId = params.get('courseId');
let token = localStorage.getItem('token');

fetch(
  `https://pure-beyond-84785.herokuapp.com/api/course/activate/${courseId}`,
  {
    method: 'PUT',
    headers: {
      Authorization: `Bearer ${token}`,
    },
  }
)
  .then((res) => res.json())
  .then((data) => {
    if (data) {
      alert('Course Activated.');
      window.location.replace('./courses.html');
    } else {
      alert('Something Went Wrong.');
      window.location.replace('./courses.html');
    }
  });
