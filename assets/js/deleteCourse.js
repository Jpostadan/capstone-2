let params = new URLSearchParams(window.location.search);
let courseId = params.get('courseId');
let userId = localStorage.getItem('userId');

let deleteContainer = document.querySelector('#deleteContainer');
let token = localStorage.getItem('token');
fetch(`https://pure-beyond-84785.herokuapp.com/api/course/archive${courseId}`, {
  method: 'DELETE',
  headers: {
    'Content-Type': 'application/json',
    Authorization: `Bearer ${token}`,
  },
}).then(async (res) => {
  const data = await res.json();
  if (data === true) {
    alert('Archived successfully');
    window.location.replace('./courses.html');
  } else {
    alert('Course is still on going');
  }
});
