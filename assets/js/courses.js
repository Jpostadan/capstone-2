//gets a string
let adminUser = localStorage.getItem('isAdmin');
let addButton = document.querySelector('#adminButton');
let adminHeader = document.querySelector('#admin');
let activeCourse = localStorage.getItem('isActive');
let token = localStorage.getItem('token');

let cardFooter;

if (adminUser === 'true') {
  adminHeader.innerHTML = ` <h1 class="text-white py-3 my-3" >Welcome !!</h1>`;
  addButton.innerHTML += `<div class="col-md-2 offset-md-10">
	<a href="../pages/addCourse.html" class="btn btn-info btn-block"> Add Course </a>
	</div>
   <h2 class="text-white py-2 my-2" >Admin Panel</h2>
	`;
} else {
  addButton.innerHTML += ' ';
}

if (adminUser === 'true' && adminUser !== null) {
  fetch('https://pure-beyond-84785.herokuapp.com/api/course/all', {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  })
    .then((res) => res.json())
    .then((data) => {
      let courseData;

      if (data.length < 1) {
        alert('No course Available');
        courseData = `<h2 class="text-info">No course Available
        <p>Please Add Course!</P>
        </h2>`;
      } else {
        courseData = data
          .map((course) => {
            if (course.isActive === true) {
              cardFooter = `<a href="./course.html?courseId=${course._id}" class="btn btn-primary text-white btn-block" id="hov">Show Course</a>
              <a href="./editCourse.html?courseId=${course._id}" value=${course._id} class="btn btn-info text-black btn-block editButton"> Edit </a>
					<a href="./deleteCourse.html?courseId=${course._id}" class="btn btn-danger text-white btn-block" id="hov">Archive Course</a>`;
            } else {
              cardFooter = ` <a href="./course.html?courseId=${course._id}" class="btn btn-primary text-white btn-block" id="hov">Show Course</a>
              <a href="./editCourse.html?courseId=${course._id}" value=${course._id} class="btn btn-info text-black btn-block editButton"> Edit </a>
					<a href="./activateCourse.html?courseId=${course._id}" class="btn btn-success text-white btn-block" id="hov">Activate Course</a>`;
            }
            return `	
					<div class="col-md-6 my-3">
						<div class="card">
							<div class="card-body bg-secondary">
								<h5 class="card-title">${course.name}</h5>
								<p class="card-text text-left">${course.description}</p>
								<p class="card-text text-right">&#8369 ${course.price}</p>
							</div>
							<div class="card-footer">
								${cardFooter}
							</div>
							
							</div>
						</div>
					</div>

				`;
          })
          .join('');
      }

      let container = document.querySelector('#coursesContainer');

      container.innerHTML = courseData;
    });
} else {
  fetch('https://pure-beyond-84785.herokuapp.com/api/course')
    .then((res) => res.json())
    .then((data) => {
      let courseData;
      if (data.length < 1) {
        alert('No course Available');
        courseData = `<h2 class="text-info">No course Available
        <p>Please try again later!</P>
        </h2>`;
      } else {
        courseData = data
          .map((course) => {
            if (token === null && adminUser === null) {
              cardFooter = `<a href="./course.html?courseId=${course._id}" class="btn btn-primary text-white btn-block">View Course</a>`;
            } else
              cardFooter = ` <a href="./course.html?courseId=${course._id}" class="btn btn-primary text-white btn-block">Go To Course</a>`;

            return `	
					<div class="col-md-6 my-3">
						<div class="card">
							<div class="card-body bg-secondary">
								<h5 class="card-title">${course.name}</h5>
								<p class="card-text text-left">${course.description}</p>
								<p class="card-text text-right">${course.price}</p>
							</div>
							<div class="card-footer">
								${cardFooter}
							</div>
							
							</div>
						</div>
					</div>
				`;
          })
          .join('');
      }

      let container = document.querySelector('#coursesContainer');

      container.innerHTML = courseData;
    });
}
